## Investigate the properties of Vrho, Vuc over parameter space (nI, rho, k)
## 1) given nI, rho, determind max K, be careful on two sqrt(..) that may be complex
## 2) the integral of Vuc is more sensitivie to zmin (~1e-5) than Vrho did (~0.01)

import numpy as np
import matplotlib.pyplot as plt
from sympy import symbols,diff,Rational,simplify, integrate
from sympy import sin,cos,exp,atan,ln, sqrt, sign, I, re, im, N, S
from sympy import cse, Eq
from sympy.utilities.lambdify import lambdify, implemented_function
from scipy import integrate
#from sympy import Function
#from sympy import sympify, count_ops
#from sympy import UnevaluatedExpr
#mport sympy.utilities.codegen
#x = symbols("x", real=True)
import random

z = symbols("z", nonnegtive=True)
ut0, rho, nI0, k = symbols("ut0 rho nI0, k", nonnegtive=True)

u  = (1+z*z)**(S(1)/3)
ft = 1 - ut0**3 / u**3
fc = 1 -      1 / u**3
Q  = ( u**(S(3)/2) * sqrt(fc) * ( 3 * rho**2 + 2 * (u**3 - 1)) / ( 2 * (u**3 - 1 + rho**2)**(S(3)/2) ) ) # 0 as u->1
q  = (9 * sqrt(u) * rho**4) / ( 4* sqrt(fc) * (u**3 -1 + rho**2)**(S(5)/2) ) ## 1/(u-1) as u->1
g1 = (  (ft * sqrt(u) * nI0 * q) / (3 * sqrt(fc) ) )  ## 1/(u-1) as u->1
g2 = ( ( sqrt(fc) * nI0 * q ) / (3 * u**(S(7)/2) ) )  ## finite as u->1

## for small rho, the maxk that keeps zeta real may not be at u=0 
## wnat to avoid the rho,k that results complex zeta,
zeta = sqrt( (1 + g1) / (1 + g2 - k**2/(u**8 * ft) + (nI0 * Q)**2 / u**5)  )  ##  may be complex  ## 1/(u-1) as u->1

kmax2 = (1 + g2 + (nI0 * Q)**2 / u**5)*(u**8 * ft)

### 
Irho =  - (3*nI0*rho**3 * (4 - 4 *u**3 + rho**2))  \
     / (8*sqrt(u) * (u**3 + rho**2 - 1.)**(S(7)/2)) \
     * ( zeta  + u**4 * (u**3 - ut0**3)  / ((u**3 - 1) * zeta) )  \
     - zeta*(3*nI0**2 *(u**3-1) *rho**3 *(2*u**3 + 3*rho**2 - 2)) \
     / ( 4*u**(S(5)/2)*(u**3 + rho**2 - 1)**4)

#p  = 2 + (S(5)/2) * (3-rho**2) / (2*u**2-1+rho**2)
#alpha = 0.75*nI0/rho
#c1 = 1/sqrt(3)*sqrt(alpha)*k/sqrt( (1-ut0**3)*(1+alpha)-k**2 )
#a0p = nI0*Q/ u**(S(5)/2) * zeta 
#Iuc1 =   (S(1)/2) * u**(S(5)/2) * (zeta*(p+2)*g2+(p-2)/zeta*g1) - nI0*q*a0p*(u**3+2)/(3*u**2) \
#      - alpha*k/(6*c1*(u-1)**(S(3)/2)) + 3*g1/(2*sqrt(u)*fc*zeta)

Iuc =  (3*nI0*rho**4) / ( 16*sqrt(u)*(u**3 + rho**2-1)**(S(7)/2)) \
     * ( (7+8*u**3+3*rho**2) *zeta - ( u**4*(u**3-ut0**3)*(21 - 11*rho**2 + u**3*(5*rho**2-21))) / ( (u**3-1)**2*zeta) )  \
     - ( 3*nI0**2*rho**4*(u**3 + 2)*(2*u**3+3*rho**2-2)) / (8*u**(S(5)/2) *(u**3 +rho**2-1)**4)*zeta \
     - sqrt( nI0*((1-ut0**3)*( 1+(3*nI0)/(4*rho)) - k**2)) / ( 4*sqrt(rho)*(u - 1)**(S(3)/2) )
     
kmax3 =  1+(3*nI0)/(4*rho)
     
Ini  =   (3*rho**4)/(8*u**(S(1)/2)*(u**3+rho**2-1)**(S(5)/2)) * ( (u**4*(u**3-ut0**3)) / ((u**3-1)*zeta) + 0*zeta) \
      + (nI0*(u**3-1)*(2*u**3+3*rho**2-1)**2) / ( 4*u**(S(5)/2)*(u**3 + rho**2-1)**3) * zeta
IO   = u**(S(5)/2) * ( zeta * (1 + g2 + (nI0*Q)**2 / u**5) - 1)
Iuc0 = k*zeta/(u**(S(11)/2)*ft)
Imu0 =   3*rho**4/(8*u**(1/2)*(u**3+rho**2-1)**(S(5)/2)) * (((u**4)*(u**3-ut0**3))/((u**3-1)*zeta) + 1.0000*zeta) \
       + (nI0*(u**3-1)*(2*u**3+3*rho**2-1)**2) / (4*u**(S(5)/2)*(u**3 +rho**2-1)**3) * zeta

Irho_z = (2*z*Irho)/(3*(1+z*z)**(S(2)/3))
Iuc_z  = (2*z*Iuc) /(3*(1+z*z)**(S(2)/3))
Ini_z  = (2*z*Ini) /(3*(1+z*z)**(S(2)/3))
Iuc0_z = (2*z*Iuc0)/(3*(1+z*z)**(S(2)/3))
Imu0_z = (2*z*Imu0)/(3*(1+z*z)**(S(2)/3))
IO_z   = (2*z*IO)  /(3*(1+z*z)**(S(2)/3))
    
    
Vrho  = lambdify( (z,k,rho,nI0,ut0), Irho_z) 
Vuc   = lambdify( (z,k,rho,nI0,ut0), Iuc_z) 
Vuc0  = lambdify( (z,k,rho,nI0,ut0), Iuc0_z) 
Vmu0  = lambdify( (z,k,rho,nI0,ut0), Imu0_z) 
VIO   = lambdify( (z,k,rho,nI0,ut0), IO_z) 

Vzeta = lambdify( (z,k,rho,nI0,ut0), zeta) 
Vmaxk2 = lambdify( (z,rho,nI0,ut0), kmax2) 
Vmaxk3 = lambdify( (rho,nI0,ut0), kmax3) 
    
#    if 0:
#        ### variables
#        vars =  [  Irho,  Irho_z,   Iuc,  Iuc_z,   Ini,  Ini_z ,  Iuc0,  Iuc0_z,   Imu0,  Imu0_z,  IO,  IO_z ]
#        varsl = [ "Irho","Irho_z", "Iuc","Iuc_z", "Ini","Ini_z", "Iuc0","Iuc0_z", "Imu0","Imu0_z","IO","IO_z" ]
#        cse_res = cse(vars)
#        
#        cpp = open("sympy2QCD.h", "w")
#        #### write temp vars
#        for i in cse_res[0]:
#            print ( "double ", ccode((i[1]), assign_to=i[0]), file=cpp)
#    
#        expr = cse_res[1]
#        for j in range(len(vars)):
#            print ( "double %s = %s;"%(varsl[j], ccode( N(expr[j]) ) ), file=cpp )
#            
#        cpp.close()


####################
#uc0 = 4* (myintegral(Iuc0_z, x_k, x_rho, nI0_B) )**2
#mu0 = myintegral(Imu0_z, x_k, x_rho, nI0_B)
#Omega0  = -2/7. - mu0*nI0 + myintegral(IO_z, x_k, x_rho, nI0_B)
#e0 = mu0*nI0 + Omega0
#uT = ut0*uc0
#uc = uc0
#nI = nI0 * uc0**(5/2)
#mu = mu0*uc0
#Omega = uc0**(7/2)* Omega0
#p  =   -Omega
#e =     uc0**(7/2)* e0
###################


def estimate_KMAX(rho,nI0,ut0):
    #return np.sqrt(1 + 0.75*nI0_B/x_rho )
    MAGICZ=0.3   
    N=64
    z=np.linspace(0,MAGICZ,N)
    zmin = 0
    min = 1.0 + 0.75*nI0/rho
    for i in range(1,N):
        tmp = Vmaxk2(z[i],rho,nI0,ut0)
        if tmp < min: 
            min=tmp
            zmin = z[i]
    return zmin, np.sqrt(min*0.99)   ## 0.99 to make sure the minimal        

## integrate in [ZMIN, 1/ZMIN]    
def myintegral_fix(ZMIN, f, k, rho, nI0, ut0=0):
    ZMID = rho*4  # for high resolution
    ZMAX = rho*200   ##1./ZMIN
    z  = np.linspace(ZMIN,ZMID,100)
    I0=0
    for i in range(100-1):
        tmp, _ = integrate.fixed_quad(f, z[i],z[i+1], args=(k, rho, nI0, ut0), n=5)
        I0 += tmp
    z2  = np.linspace(ZMID,ZMAX,200)
    I1=0
    for i in range(200-1):
        tmp, _ = integrate.fixed_quad(f, z2[i],z2[i+1], args=(k, rho, nI0, ut0), n=7)
        I1 += tmp
        
    return I0 + I1

## integrate in [ZMIN, 1/ZMIN]    
def myintegral_qua(ZMIN, f, k, rho, nI0, ut0=0):
    ZMID = rho*4  # for high resolution
    ZMAX = rho*500   ##1./ZMIN
    #ZMAX = 1./ZMIN
    #ZMAX = 2000
    I0, _ = integrate.quadrature(f, ZMIN,ZMID, args=(k, rho, nI0, ut0), tol = 1e-4)
    I1, _ = integrate.quadrature(f, ZMID,ZMAX, args=(k, rho, nI0, ut0), tol = 1e-4)
    return I0 + I1

def myintegral(ZMIN, f, k, rho, nI0, ut0=0):
    ZMID = rho*4  # for high resolution
    ZMAX = rho*500   ##1./ZMIN
    #ZMAX = 1./ZMIN
    #ZMAX = 2000
    NP=1024*4
    z  = np.linspace(ZMIN,ZMID,NP)
    I0 = integrate.simps( f(z,  k, rho, nI0, ut0),  z )
    z2  = np.linspace(ZMID,ZMAX,NP*2)
    I1 = integrate.simps( f(z2, k, rho, nI0, ut0), z2 )
    return I0 + I1


