from plotQCD import plotQCD

import numpy as np
from scipy.optimize import curve_fit

import matplotlib as mpl
from matplotlib.font_manager import FontProperties
mpl.rcParams.update({'font.size': 14})
mpl.rcParams['legend.numpoints'] = 2
mpl.rc('font', family='serif')
mpl.rcParams['axes.xmargin'] = 0
mpl.rcParams['axes.ymargin'] = 0
mpl.rcParams['legend.fontsize'] = 'large'
import matplotlib.pyplot as plt


data = np.loadtxt('state_great2.dat')
#data = np.loadtxt('state.dat')
#plotQCD(data)

def poly1(p, k1, g1):
    return k1 * p ** g1
def poly2(p, c1, k1, g1):
    return c1 + k1 * p ** g1
def dpoly(x, k1,g1, k2, g2):
    return  k1 * x ** g1 + k2 * x ** g2

if 0:
    z=np.linspace(0,2,100)
    plt.plot(z, (z)**0.8)
    sdfs

##==============================
B, LA=100, -137
B, LA=180, -130
B, LA=180, -139
#B, LA=50, -1
p   =  data[B:LA,8]
e   =  data[B:LA,10]

print (p[-1],e[-1])

p = p - p[-1]
e = e - e[-1]

#######
func = dpoly
X,Y=p[:],e[:]

plt.figure(figsize=(14,10))
plt.subplot(221)
popt, _ = curve_fit(func, X, Y )
plt.plot(X[:], Y[:], '.')
plt.plot(X, func(X, *popt), 'r-', label="a1=%.3f  g1=%.3f \na2=%.3f g2=%.3f"%tuple(popt))
plt.legend()
plt.xlabel("p")
plt.ylabel("e")

##==============================
B, LA=90, -138
B, LA=48, -2
p   =  data[B:LA,8]
e   =  data[B:LA,10]
p = p - p[-1]
e = e - e[-1]
#######
func = dpoly
X,Y=p[:],e[:]

plt.figure(figsize=(14,10))
plt.subplot(221)
popt, _ = curve_fit(func, X, Y )
plt.plot(X[:], Y[:], '.')
plt.plot(X, func(X, *popt), 'r-', label="a1=%.3f  g1=%.3f \na2=%.3f g2=%.3f"%tuple(popt))
plt.legend()
plt.xlabel("p")
plt.ylabel("e")


# =============================================================================
#mu  = data[B:LA,7]
#p   =  data[B:LA,8]
#e   =  data[B:LA,10]
#rho =  data[B:LA,1]

# 
# ###
# func = dpoly
# X,Y=p,rho
# 
# plt.figure(figsize=(14,10))
# plt.subplot(221)
# popt, _ = curve_fit(func, X, Y )
# plt.plot(X, func(X, *popt), 'r-', label=tuple(popt))
# plt.plot(X[:], Y[:], '.')
# plt.legend()
# plt.xlabel("p")
# plt.ylabel("rho")
# =============================================================================

