import QCD

import numpy as np
import matplotlib as mpl
from matplotlib.font_manager import FontProperties
mpl.rcParams.update({'font.size': 18})
mpl.rc('font', family='serif')

import matplotlib.pyplot as plt
from scipy import integrate

### Test kmax dependency on nI rho
if 0:
    nlist = np.linspace(0.0, 2, 32)
    rho = .1
    klist = []
    for nI in nlist:
        _, kmax = QCD.estimate_KMAX(rho,nI,0)
        klist.append(kmax)
    plt.plot(nlist, klist, label="rho = %.1f"%rho)

    rho = .3
    klist = []
    for nI in nlist:
        _, kmax = QCD.estimate_KMAX(rho,nI,0)
        klist.append(kmax)
    plt.plot(nlist, klist, label="rho = %.1f"%rho)

    rho = 1
    klist = []
    for nI in nlist:
        _, kmax = QCD.estimate_KMAX(rho,nI,0)
        klist.append(kmax)
    plt.plot(nlist, klist, label="rho = %.1f"%rho)

    rho = 2
    klist = []
    for nI in nlist:
        _, kmax = QCD.estimate_KMAX(rho,nI,0)
        klist.append(kmax)
 
    plt.plot(nlist, klist, label="rho = %.1f"%rho)
    plt.xlabel("nI")
    plt.ylabel("max k")
    plt.legend()


### Test convergence of integral
if 0:
    rho, nI = 1, 1
    _,k = QCD.estimate_KMAX(rho,nI,0)
    k *= 0.9
 
    ZMIN = 1e0
    I10=0
    I20=0
    f = lambda z: QCD.Vrho (z,k, rho, nI, 0.0)
    g = lambda z: QCD.Vuc  (z,k, rho, nI, 0.0)
    for i in range(10):
        I1 =   QCD.myintegral    (ZMIN, QCD.Vrho, k, rho, nI)
        I2 =   QCD.myintegral    (ZMIN, QCD.Vuc , k, rho, nI)
        res1 = QCD.myintegral_qua(ZMIN, QCD.Vrho, k, rho, nI)
        res2 = QCD.myintegral_qua(ZMIN, QCD.Vuc , k, rho, nI)
        #z = np.linspace(ZMIN,ZMAX,1024)
        #I1 = integrate.simps( QCD.Vrho(z,k, rho, nI, 0.0), z )
        #I2 = integrate.simps( QCD.Vuc (z,k, rho, nI, 0.0), z )
        #res1 = integrate.quadrature(f, ZMIN,ZMAX)
        #res2 = integrate.quadrature(g, ZMIN,ZMAX)
        print ("%10g %10g (%10g) %10g (%10g)" % (ZMIN, I1, res1, I2, res2 ) ) 
        ZMIN *= 0.25
        
    wwww

### Max k dependency 
if 0:
    rho, nI = 0.3, 0.8
    plt.figure()
    z = np.linspace(1e-5,.5,100)
    plt.plot( z, QCD.Vmaxk2(z,rho, nI, 0.0 ))
    x,y=QCD.estimate_KMAX(rho, nI, 0.0)
    plt.scatter([0,x], [QCD.Vmaxk3( rho, nI, 0.0 ), y**2 ] )
    plt.xlabel("z-Maxk")



ZMIN1 = 1e-4  # for Vrho  
ZMIN2 = 0.001 # for Vuc  !!! sensitive !!
  
def plot_n_k():
    Z1= 3
    nI = 2
    k=.01
    plt.figure()
    z = np.linspace(1e-6, Z1,100)
    plt.plot( z, QCD.Vrho(z, k, 0.04, nI, 0.0 ))
    plt.plot( z, QCD.Vrho(z, k, 0.05,  nI, 0.0 ))
    plt.plot( z, QCD.Vrho(z, k, 0.1,  nI, 0.0 ))
    plt.xlabel("z, Vrho -- for ~max k")
    
    plt.figure()
    z = np.linspace(1e-4, Z1,100)
    #plt.plot( z, Vuc(z, k, 0.005,  nI, 0.0 ))
    plt.plot( z, QCD.Vuc(z, k, 0.5,  nI, 0.0 ))
    plt.plot( z, QCD.Vuc(z, k, 0.3,  nI, 0.0 ))
    #plt.plot( z, Vuc(z, k, 0.5,  nI, 0.0 ))
    plt.xlabel("z, Vuc")   

##plot_n_k()

### Irho, Iuc plot    
if 1:
    FACTOR = 2
    plt.figure(figsize=(10,3))
    plt.subplot(121)
    rho, nI = 0.001, 1
    Z1= rho*FACTOR
    zzz, KMAX = QCD.estimate_KMAX(rho, nI, 0.0)
    k = 0.1*KMAX
    z = np.linspace(1e-3, Z1, 1000)
    plt.plot( QCD.Vrho(z, k, rho, nI, 0.0 ))
    plt.xlabel("z, Vrho -- for ~max k")

    plt.subplot(122)
    z = np.linspace(1e-3, Z1, 1000)
    plt.plot( QCD.Vuc(z, k, rho, nI, 0. ) )
    plt.xlabel("z, Vuc")
    
    #z = np.linspace(Z1, rho*3,1000)
    plt.figure(figsize=(10,3))
    plt.subplot(121)
    rho, nI = 0.1,1
    Z1 = rho*FACTOR
    zzz, KMAX = QCD.estimate_KMAX(rho, nI, 0.0)
    k = 0.9*KMAX
    z = np.linspace(1e-3, Z1, 1000)
    plt.plot( QCD.Vrho(z, k, rho, nI, 0.0 ))
    plt.xlabel("z, Vrho -- for ~max k")
    
    plt.subplot(122)
    z = np.linspace(1e-11, Z1, 1000)
    plt.plot( QCD.Vuc(z, k, rho, nI, 0. ) )
    plt.xlabel("z, Vuc")
    
    sdf

#    plt.figure()
#    z = np.linspace(1e-4, Z1,100)
#    plt.plot( z, Vmaxk2(z, rho, nI, 0. ) )
#    plt.plot( [0,zzz], [Vmaxk3(rho, nI, 0. ), KMAX], 'o' )
#    plt.xlabel("z, maxk")


##================================
#print (Vuc (1e-7,.5,.5,.4,0) )
#ki = 0.6
#ri = 0.6
#d = 0.01
#for i in range(500):
#    myintegral(Vrho, ki+d, ri+d)
#    RHO[i,j] = integrate.simps( Vuc (z,x_k, x_rho, 0.00, 0.0), z )
#    print ( UC [i,j], RHO[i,j] ) 
#    print ( "--" ) 
###

P_CUT_UC  = 1e-4    ###
P_CUT_RHO = 1e-4
        
    
####
#### Grid search, 0-cross appears on large-k, small rho usually....
####
NI  = NJ = 40
UC  = np.zeros([NI,NJ])
RHO = np.zeros([NI,NJ])

nI0_B = 32
#for nI0_B in np.linspace(0.05,3.05, 16):
#for nI0_B in np.linspace(5, 20, 50):
for nI0_B in np.linspace(0, 20, 50):
    print ("Processing nI0=", nI0_B)
    for i in range(NI):
        x_rho = 0.05 + 3*i/NI
        _, KMAX = QCD.estimate_KMAX(x_rho,nI0_B,0)
        #print (zmax,KMAX)
        for j in range(NJ):
          x_k   = KMAX*0.2 + (KMAX*0.8 )*j/NJ
          RHO[i,j] = ( QCD.myintegral_qua(P_CUT_UC, QCD.Vrho, x_k, x_rho, nI0_B)  )
          UC [i,j] = ( QCD.myintegral_qua(P_CUT_UC, QCD.Vuc,  x_k, x_rho, nI0_B) )      

    plt.figure(figsize=(10,5))
    plt.subplot(121)
    plt.imshow(RHO>=0, aspect=1, interpolation='nearest', origin='upper')
    plt.title("%f : %f"%(np.min(RHO), np.max(RHO)))
    plt.ylabel("rho")
    plt.xlabel("k - Vrho")

    plt.subplot(122)
    plt.imshow(UC>=0,  aspect=1, interpolation='nearest', origin='upper')
    plt.title("%f : %f"%(np.min(UC), np.max(UC)))
    plt.xlabel("k - Vuc (nI=%f)"%nI0_B)
    
    plt.savefig("VE%04.2f.png"%nI0_B)
    plt.cla()
    plt.clf()