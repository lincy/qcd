import numpy as np
import matplotlib.pyplot as plt
from sympy import symbols,diff,Rational,simplify, integrate
from sympy import sin,cos,exp,atan,ln, sqrt, sign, I, re, im, N, S
from sympy import cse, Eq
from sympy.utilities.lambdify import lambdify, implemented_function
from scipy import integrate
#from sympy import Function
#from sympy import sympify, count_ops
#from sympy import UnevaluatedExpr
#mport sympy.utilities.codegen
#x = symbols("x", real=True)
import random

u = symbols("u", nonnegtive=True)
nI0 = symbols("nI0", nonnegtive=True)

k0 = sqrt( 8*nI0**2 / 9. + 1 )
g  = (u**8 + u**3 * nI0 **2) - k0**2

I_uc0 = k0 /(u**(3/2) * sqrt(g))
I_mu0 = u**(3/2) / sqrt(g)
I_p0  = u**(13/2) / sqrt(g) - u**(5/2)
I_O  = u**(13/2) / sqrt(u**8-1) - u**(5/2)
    
V_uc0 = lambdify( (u,nI0), I_uc0) 
V_mu0 = lambdify( (u,nI0), I_mu0) 
V_p0  = lambdify( (u,nI0), I_p0) 
V_O   = lambdify( (u,nI0), I_O) 

l = 1


## integrate in [ZMIN, 1/ZMIN]    
def myintegral_qua(ZMIN, f, nI0):
    ZMAX = 1./ZMIN
    I0, _ = integrate.quadrature(f, 1+ZMIN, ZMAX, args=(nI0), tol = 1e-5)
    return I0

def myintegral(ZMIN, f, nI0):
    ZMAX = 1./ZMIN
    NP=1024*1000
    z  = np.linspace(1+ZMIN,ZMAX,NP)
    I0 = integrate.simps( f(z, nI0),  z )
    return I0

delta = 0.001
nI0_=0


z = np.linspace(1, 1/delta, 100)
plt.plot(V_uc0(z,0) )

uc0 = ( 2*myintegral(delta, V_uc0, nI0_) )**2
mu0 = 1/3. + nI0_ * myintegral(delta, V_mu0, nI0_)
p0  = 2/7. - myintegral(delta, V_p0, nI0_)
e0  = mu0*nI0_ - p0
Om  = uc0**3.5* (-2/7. + myintegral(delta, V_O, nI0_)  )

uc = uc0/(l*l)
nI = uc0**(5/2)*nI0_
mu = uc0/(l*l)*mu0
p  = uc0**(7/2)/(l**7)*p0
O  = -p
e  = uc0**(7/2)/(l**7)*e0

print(uc, nI, mu, p, O, e)
