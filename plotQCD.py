import matplotlib as mpl
from matplotlib.font_manager import FontProperties
##mpl.rcParams.update({'font.size': 14})
mpl.rcParams['legend.numpoints'] = 2
mpl.rc('font', family='serif')
#mpl.rcParams['axes.xmargin'] = 0
#mpl.rcParams['axes.ymargin'] = 0
mpl.rcParams['legend.fontsize'] = 'small'

import matplotlib.pyplot as plt

#state.append([0x_k, 1x_rho, 2Vr, 3Vu, 4uT, 5uc, 6nI, 7mu, 8p, 9Omega, 10 e])
def plotQCD(x):
       
    plt.figure(1,figsize=(20,8))
    plt.subplot(241)
    #plt.xlim([0,1])
    plt.xlabel("mu")
    plt.plot(x[:,7],x[:,9],'.', label="Omega")
    plt.legend()
    plt.subplot(242)
    plt.xlabel("mu")
    #plt.xlim([0,1])
    plt.plot(x[:,7],x[:,6],'.', label="nI")
    plt.legend()
    plt.subplot(243)
    plt.xlabel("mu")
    plt.plot(x[:,7],x[:,1],'.', label="rho")
    plt.legend()
    plt.subplot(245)
    plt.xlabel("mu")
    #plt.xlim([0,1])
    plt.plot(x[:,7],x[:,5],'.', label="uc")
    plt.legend()
    plt.subplot(246)
    plt.xlabel("mu")
    #plt.xlim([0,1])
    plt.plot(x[:,7],x[:,8],'.', label="p")
    plt.legend()
    plt.subplot(247)
    plt.xlabel("mu")
    #plt.xlim([0,1])
    plt.plot(x[:,7],x[:,10],'.', label="e")
    plt.legend()

    plt.subplot(244)
    plt.xlabel("k")
    plt.plot(x[:,0],x[:,2],'.', label="Vrho")
    plt.plot(x[:,0],x[:,3],'.', label="Vuc")
    plt.legend()
    plt.subplot(248)
    plt.xlabel("rho")
    plt.plot(x[:,1],x[:,2],'.', label="Vrho")
    plt.plot(x[:,1],x[:,3],'.', label="Vuc")
    plt.legend()
    plt.savefig("mu_deconfined.jpg")

    plt.cla()
    plt.clf()
    
    plt.figure(3)
    plt.xlabel("p")
    plt.plot(x[:,8],x[:,10],'.', label="e")
    plt.legend()
    plt.savefig("p_e.jpg")
    plt.cla()
    plt.clf()
    



#txt = np.loadtxt('state0.dat')
#plots(txt)
