import QCD    ## where we define all the equations
from plotQCD import plotQCD

import numpy as np
from numpy import sqrt
import matplotlib.pyplot as plt
from scipy import integrate
import random

### Test convergence of integral
if 0:
    rho, nI = 0.1, 0.2
    _,k = QCD.estimate_KMAX(rho,nI,0)
    k *= 0.98
 
    ZMIN = 1e-3
    I10=0
    I20=0
    for i in range(20):
        I1 =   QCD.myintegral    (ZMIN, QCD.Vrho, k, rho, nI)
        I2 =   QCD.myintegral    (ZMIN, QCD.Vuc , k, rho, nI)
        res1 = QCD.myintegral_qua(ZMIN, QCD.Vrho, k, rho, nI)
        res2 = QCD.myintegral_qua(ZMIN, QCD.Vuc , k, rho, nI)
        print ("%10g %10g (%10g) %10g (%10g)" % (ZMIN, I1, res1, I2, res2 ) ) 
        #print (ZMIN, I1, res1, I2, res2)
        ZMIN = ZMIN*0.5
    asdasd
##================================
####
#### SGD search
####

P_MIN_RHO, P_MAX_RHO  = 0.1, .05     ## BRANCH1
P_MIN_RHO, P_MAX_RHO  = 0.3, 1       ## BRANCH2

P_MIN_K   = 0.6
    
P_CUT_UC  = 1e-4    ###
P_CUT_RHO = 1e-4    ### 

P_STOP_RHO = 1e-6   ##0.01/ZMAX2
P_STOP_UC  = P_STOP_RHO

P_ITER_RETRY  = 1000
P_ITER_ABORT  = 3000

P_FACTOR = 0.1

ut0 = 0

x_rho =   0.655
x_rho = (P_MIN_RHO + P_MAX_RHO ) /2.
x_rho = 0.60246
_, KMAX = QCD.estimate_KMAX(x_rho, 0.01,0)
x_k   =   0.9002  ##0.8 * KMAX
x_k   =   0.921983

state = []

#r0 = k0 = 9.9

#for nI0 in np.linspace(0.08, 3.0, 32):
for nI0 in np.linspace(1.23540 , 0.8, 256):
#for nI0 in np.linspace(0.05, 1.4, 64):
    #print ("Processing nI0 = %10f, start from k,rho= %f %f" % (nI0,x_k, x_rho) )

    _, KMAX = QCD.estimate_KMAX(x_rho,nI0,0)
    DR =  0.001       ## finite difference
    DK = KMAX / 1000.
    iters = 0

    while (True):
        iters = iters + 1

        UC    = QCD.myintegral(P_CUT_UC , QCD.Vuc,  x_k, x_rho, nI0)      
        RHO   = QCD.myintegral(P_CUT_RHO, QCD.Vrho, x_k, x_rho, nI0)
        
        if ( (abs(RHO) < P_STOP_RHO ) and (abs(UC) < P_STOP_UC )):
            
            uc0 = 4* (QCD.myintegral(P_CUT_UC, QCD.Vuc0, x_k, x_rho, nI0) )**2
            mu0 =     QCD.myintegral(P_CUT_UC, QCD.Vmu0, x_k, x_rho, nI0)
            Omega0  = -2./7 - mu0*nI0 + QCD.myintegral(P_CUT_UC, QCD.VIO, x_k, x_rho, nI0)
            e0 = mu0*nI0 + Omega0
            uT = ut0*uc0
            uc = uc0
            nI = nI0 * uc0**(5./2)
            mu = mu0*uc0
            Omega = uc0**(7./2)* Omega0
            p  =   -Omega
            e =     uc0**(7./2)* e0
        
            state.append([x_k, x_rho, RHO, UC, uT, uc, nI, mu, p, Omega, e])
            print (" --- Found (nI,k,rho)=%5f %5f %5f, (Vrho,Vuc)=%5f %5f" %( nI0, x_k, x_rho, RHO, UC))
            print (" ---      uc= %5f, mu= %5f, p=-Omega= %5f" %( uc, mu, p ))

            break

        if (iters%P_ITER_RETRY == 0):
            x_rho = random.uniform(P_MIN_RHO,P_MAX_RHO)
            _, KMAX = QCD.estimate_KMAX(x_rho,nI0,0)
            x_k   = random.uniform(P_MIN_K, KMAX)
            print (" Retry ..." )
            continue
        if (iters > P_ITER_ABORT):
            break
        
        ## backward finite difference...
        RHO_k = QCD.myintegral(P_CUT_RHO, QCD.Vrho, x_k - DK, x_rho, nI0)
        UC_k  = QCD.myintegral(P_CUT_UC , QCD.Vuc,  x_k - DK, x_rho, nI0)
        _, KMAX = QCD.estimate_KMAX(x_rho - DR,nI0,0)
        x_k0 = min(KMAX, x_k)
        RHO_r = QCD.myintegral(P_CUT_RHO, QCD.Vrho, x_k0, x_rho - DR, nI0)
        UC_r  = QCD.myintegral(P_CUT_UC, QCD.Vuc,  x_k0, x_rho - DR, nI0)      
                
        ## estimate d_rho, dk by weighted averaging of the gradient
        w_uc = 0.5  ##abs(UC) / ( abs(RHO) + abs(UC))
        dk = ( DK*RHO/(RHO-RHO_k)*(1-w_uc) + DK*UC/(UC-UC_k)*w_uc ) * P_FACTOR
        dr = ( DR*RHO/(RHO-RHO_r)*(1-w_uc) + DR*UC/(UC-UC_r)*w_uc ) * P_FACTOR
        
        while (x_rho - dr < P_MIN_RHO or x_rho - dr > P_MAX_RHO):
            dr = dr * 0.5

        x_rho = x_rho - dr
        _, KMAX = QCD.estimate_KMAX(x_rho, nI0, 0)
        while (x_k - dk < P_MIN_K or x_k - dk > KMAX):
            dk = dk * 0.5

        x_k   = x_k - dk

        ##if (abs(x_rho - x_rho0) < 0.00001 or abs(x_k - x_k0) < 0.00001 ):
        if (iters%20==0):
            print ("%15f %15f %15f %15f"% (x_rho, x_k, RHO, UC )  )
  
    
    ## output every nI-step        
    data = np.array(state)
    np.savetxt('state.dat', data)
    plotQCD( data )
    

    


#
#
#          
#    plt.figure()
#    plt.imshow(RHO>=0, aspect=1, interpolation='nearest', origin='upper')
#    plt.title("%f : %f"%(np.min(RHO), np.max(RHO)))
#    plt.xlabel("k - Vrho (nI=%f)"%nI0_B)
#    
#    plt.figure()
#    plt.imshow(UC>=0,  aspect=1, interpolation='nearest', origin='upper')
#    plt.title("%f : %f"%(np.min(UC), np.max(UC)))
#    plt.xlabel("k - Vuc (nI=%f)"%nI0_B)
#    #plt.imshow(UC>0,  aspect=1, interpolation='nearest', origin='upper')
